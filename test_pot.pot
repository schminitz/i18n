msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-02-06 13:03+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: fr\n"
"Language-Name: French\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: DOMAIN\n"

#. Default: "De 0 &agrave; 15 jours de pr&eacute;sence: les animaux sont commercialisables vers n'importe quel type d'exploitation, m&ecirc;me exportation."
#: arsia/cerise/negotrans/browser/templates_negociant/negociantinventaire.pt:25
msgid "0_a_15"
msgstr ""

#. Default: "De 16 &agrave; 30 jours: les animaux peuvent seulement &ecirc;tre vendus vers une exploitation en Belgique donc plus export&eacute;."
#: arsia/cerise/negotrans/browser/templates_negociant/negociantinventaire.pt:28
msgid "16_a_30"
msgstr ""

#. Default: "(sur les <b>4 premiers</b> ou les <b>4 derniers chiffres de la boucle</b>)"
#: arsia/cerise/saniweb/skins/cerise_saniweb_skin/bovin_naissance/naissance.cpt:98
#: arsia/cerise/saniweb/skins/cerise_saniweb_skin/bovin_naissance/naissance_2.cpt:96
msgid "4-chiffres"
msgstr "resource update 2"

#. Default: "Veuillez entrer 4 chiffres pour la recherche"
#: arsia/cerise/saniweb/browser/KSSActions.py:50
msgid "4-chiffres-pour-recherche"
msgstr "4 chiffres pour recherche traduit dans pot"

#. Default: "(vous devez indiquer au minimum les 4 derniers chiffres de la boucle et vous ne devez pas indiquer le check-digit)"
#: arsia/cerise/saniweb/skins/cerise_saniweb_skin/bovin_naissance/naissance.cpt:414
msgid "4-chiffres2"
msgstr "resource update"

#. Default: "Gigigigig"
#: arsia/cerise/core/translateCodes.py:521
msgid "ALIVE"
msgstr "Vivant traduit dans pot"
